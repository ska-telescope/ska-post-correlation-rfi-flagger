# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/engs1536/CLionProjects/ska-post-correlation-rfi-flagger/main.cpp" "/Users/engs1536/CLionProjects/ska-post-correlation-rfi-flagger/cmake-build-debug/CMakeFiles/ska-post-correlation-rfi-flagger.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "AppleClang")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/Users/engs1536/software/include"
  "/usr/local/Cellar/hdf5/1.12.0_4/include"
  "/usr/local/opt/szip/include"
  "/Users/engs1536/software/include/casacore"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
