import platform
from setuptools import setup, Extension


extra_compile_args = ["-Wall", "-Wextra", "-O3", "-std=c++11"]
python_module_link_args = []

if platform.system() == "Darwin":
    extra_compile_args += ["-mmacosx-version-min=10.9"]
    python_module_link_args += ["-ldl"]
else:
    python_module_link_args += ["-Wl,-rpath,$ORIGIN"]
    python_module_link_args += ["-ldl"]

class get_numpy_include(object):
    """Defer numpy.get_include() until after numpy is installed."""

    def __str__(self):
        import numpy
        return numpy.get_include()


def get_extension_modules():
    return [
        Extension(
            "ska_post_correlation_rfi_flagger",
            sources=[
                "python_wrapper.cpp",
            ],
            depends=["setup.py"],
            include_dirs=["./", get_numpy_include()],
            extra_compile_args=extra_compile_args,
            extra_link_args=python_module_link_args
        )
    ]

setup(
    name="ska-post-correlation-rfi-flagger",
    version="0.0.2",
    description="Post correlation RFI flagger module",
    packages=[],
    setup_requires=["numpy"],
    ext_modules=get_extension_modules(),
    install_requires=["numpy"],
)
