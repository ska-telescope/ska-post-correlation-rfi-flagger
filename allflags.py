import casacore.tables as tables
import numpy as np



vis = tables.table('~/CLionProjects/ska-post-correlation-rfi-flagger/cmake-build-debug/MeasurementSets/20_sources_no_screen/'
                   'aa0.5_ms_without_screens.MS', readonly = False)
rfi = tables.table('~/CLionProjects/ska-post-correlation-rfi-flagger/cmake-build-debug/MeasurementSets/aa05_low_rfi_84chans.ms')

num_rows = vis.nrows()

num_baselines = 21
num_antennas = 6
num_pols = 4
num_time_samples = int(num_rows / num_baselines)

num_freqs = 200

num_time_samples = int(num_rows/num_baselines)
print(num_rows)
print(num_time_samples)

baselines = vis.getcol('UVW', 0, num_baselines)
ant_id1 = vis.getcol('ANTENNA1', 0, num_baselines)
ant_id2 = vis.getcol('ANTENNA2', 0, num_baselines)
baseline_id = 0

scan_number = vis.getcol('SCAN_NUMBER', baseline_id, num_time_samples, num_baselines)
field_id = vis.getcol('FIELD_ID', baseline_id, num_time_samples, num_baselines)

# change_locations = []
# for i in range(1, num_time_samples):
#     if scan_number[i] != scan_number[i - 1] or field_id[i] != field_id[i - 1]:
#         change_locations.append(i)
#
# print(change_locations)


def weight_gen(baseline_locs, ant1, ant2, thresh):
    weights = []
    pairs = []
    for i in range(num_baselines):
        b = baseline_locs[i]
        a = np.zeros(2)
        dist = np.sqrt(np.sum(np.power(b, 2)))
        if 0 < dist < thresh:
            a[0] = ant1[i]
            a[1] = ant2[i]
            pairs.append(a)
            weights.append(1 / dist)
    return weights, pairs


def moran_corr(x, weights, pairs):
    N = len(x)
    x_avg = np.mean(x)
    sum_up = 0
    W = np.sum(weights)
    sum_down = np.sum(np.power(x - x_avg, 2))
    for i in range(N):
        sum_up = sum_up + weights[i] * (x[int(pairs[i][0])] - x_avg) * (x[int(pairs[i][1])] - x_avg)
    moran_i = (N * sum_up) / (W * sum_down)
    # print("-------------------")
    # print("N = ", N)
    # print("W = ", W)
    # print("pairs: ", pairs)
    # print("x: ", x)
    # print("x_avg = ", x_avg)
    # print("weights: ", weights)
    # print("moran's i = ", moran_i)
    # print("-------------------")
    return moran_i

def geary_corr(x, weights, pairs):
    N = len(x)
    x_avg = np.mean(x)
    sum_up = 0
    W = np.sum(weights)
    sum_down = np.sum(np.power(x - x_avg, 2))
    for i in range(N):
        sum_up = sum_up + weights[i] * (x[int(pairs[i][0])] - x[int(pairs[i][1])]) * (x[int(pairs[i][0])] - x[int(pairs[i][1])])
    geary = ((N - 1) * sum_up) / (2 * W * sum_down)
    return geary


def spc_flagger(x1, x2, weights, pairs, flags, mni, thresh_val, num_freq, mode, time):
    state = np.zeros(num_freq)
    if mode == "default":
        for j in range(1, num_freq):
            # print(moran_corr(x2[:, j], weights, pairs))
            # flags[time][j] = int(moran_corr(x2[:, j], weights, pairs) > thresh_val)
            mni[time][j] = moran_corr(x2[:, j], weights, pairs)
            if (mni[time][j] - mni[time - 1][j])/mni[time][j] > thresh_val or \
                (flags[time - 1][j] == 1 and (mni[time][j] - mni[time - 1][j])/mni[time][j] > thresh_val):
                flags[time][j] = 1

    if mode == "differential":
        x_diff = abs(x2 - x1)
        for j in range(num_freq):
            flags[time][j] = int((moran_corr(x_diff[:, j], weights, pairs) > thresh_val and np.sum(x_diff[:, j]) > 0)
                                 or (moran_corr(x_diff[:, j], weights, pairs) < thresh_val and state[j] == 1))
            state[j] = flags[time][j]
    if mode == "hybrid":
        x_diff = abs(x2 - x1)
        for j in range(num_freq):
            flags[time][j] = int((moran_corr(x_diff[:, j], weights, pairs) > thresh_val and np.sum(x_diff[:, j]) > 0)
                                 or (moran_corr(x_diff[:, j], weights, pairs) < thresh_val and state[j] == 1) or
                                 moran_corr(x2[:, j], weights, pairs) > thresh_val)
            state[j] = flags[time][j]


def extrapolator(x):
    diff11 = x[2] - x[1]
    diff12 = x[1] - x[0]
    diff2 = diff11 - diff12
    diff_e = diff11 + diff2
    x_e = x[2] + diff_e
    return x_e


def extrapol_flagger(vis_data, flags, num_freq, num_time, threshold):
    for j in range(num_freq):
        for i in range(3, num_time):
            x = np.zeros(3)
            x[0] = vis_data[i - 3][j][0]
            x[1] = vis_data[i - 2][j][0]
            x[2] = vis_data[i - 1][j][0]
            x_e = extrapolator(x)
            if (vis_data[i][j][0] - x_e) / x_e > threshold:
                flags[i][j] = 1
            if (vis_data[i][j][0] - x_e) / x_e < -threshold:
                k = 1
                while flags[i - k][j] == 0 and i - k > 0:
                    flags[i - k][j] = 1
                    k = k + 1


def twosmf(vis_data, flags, factor):
    visvis = vis_data[:, :, 0]
    [num_time, num_freq] = visvis.shape
    s = 0
    for j in range(num_freq):
        state = np.zeros(num_time)
        for i in range(1, num_time):
            var = (visvis[i][j] - visvis[i - 1][j]) / visvis[i - 1][j]
            if var > factor or (state[i - 1] == 1 and abs(var) < factor):
                state[i] = 1
                flags[i][j] = 1
                s = s + 1
            if var < 0 and abs(var) > factor and state[i - 1] == 0:
                m = i - 1
                while m > 0 and state[m] == 0:
                    flags[m][j] = 1
                    state[m] = 1
                    s = s + 1
                    m = m - 1


start_time = 0
length = 240
start_row = start_time
mode = "default"

flags = np.zeros((length, num_freqs))
mni = np.zeros((length, num_freqs))

radius = 10000
threshold_spc = 0.8
threshold_extrpl = 0.015
threshold_2sm = 0.015

weights, pairs = weight_gen(baselines, ant_id1, ant_id2, radius)

for i in range(1, length):
    print(i)
    rec2 = abs(vis.getcol('DATA', i * num_baselines, num_antennas) + rfi.getcol('DATA', i * num_baselines, num_antennas))
    rec1 = abs(vis.getcol('DATA', (i - 1) * num_baselines, num_antennas) + rfi.getcol('DATA', (i - 1) * num_baselines, num_antennas))
    spc_flagger(rec1[:, :, 0], rec2[:, :, 0], weights, pairs, flags, mni, threshold_spc, num_freqs, mode, i)

s1 = np.sum(flags)
rec_data = abs(vis.getcol('DATA', start_row, length, num_baselines) + rfi.getcol('DATA', start_row, length, num_baselines))
rfi_data = abs(rfi.getcol('DATA', start_row, length, num_baselines))
#
# twosmf(rec_data, flags, threshold_2sm)
# s2 = np.sum(flags)
#
# extrapol_flagger(rec_data, flags, num_freqs, length, threshold_extrpl)
# s3 = np.sum(flags)
# #
# print(s1)
# np.savetxt("new_results_sim/L240_R100_40-015-015_m1.txt", flags)
np.savetxt("new_results_sim/moran_distribution_40.txt", mni)

TP = 0
FP = 0
TN = 0
FN = 0
Tot = num_freqs * num_time_samples




for i in range(num_time_samples):
    for j in range(num_freqs):
        if rfi_data[i][j][0] == 0 and flags[i][j] == 0:
            TN = TN + 1
        if rfi_data[i][j][0] == 0 and flags[i][j] == 1:
            FP = FP + 1
        if rfi_data[i][j][0] > 0 and flags[i][j] == 0:
            FN = FN + 1
        if rfi_data[i][j][0] > 0 and flags[i][j] == 1:
            TP = TP + 1
print("TP : ", TP)
print("FP : ", FP)
