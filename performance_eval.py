import casacore.tables as tbl
import numpy as np

vis = tbl.table(
    '~/CLionProjects/ska-post-correlation-rfi-flagger/cmake-build-debug/MeasurementSets/20_sources_with_screen/aa0'
    '.5_ms.MS')
rfi = tbl.table(
    '~/CLionProjects/ska-post-correlation-rfi-flagger/cmake-build-debug/MeasurementSets/aa05_low_rfi_84chans.ms')

col_vis = tbl.tablecolumn(vis, 'DATA')
col_rfi = tbl.tablecolumn(rfi, 'DATA')

col_flag = tbl.tablecolumn(vis, 'FLAGS')

num_rows = vis.nrows()

