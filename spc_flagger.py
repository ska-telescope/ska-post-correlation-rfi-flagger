import casacore.tables as tables
import numpy as np

# import datetime

vis = tables.table('/mnt/RFI1/MeerKAT/Nest200047/1643124898_sdp_l0.ms')

# start_time = datetime.datetime.now()
num_rows = vis.nrows()
rfi_threshld = 0.02

batch_length = 10
num_baselines = 1830
num_antennas = 60
num_pols = 4
num_time_samples = int(num_rows / num_baselines)

num_freqs = 4096

baselines = vis.getcol('UVW', 0, num_baselines)
ant_id1 = vis.getcol('ANTENNA1', 0, num_baselines)
ant_id2 = vis.getcol('ANTENNA2', 0, num_baselines)


def weight_gen(baseline_locs, ant1, ant2, thresh):
    weights = []
    pairs = []
    for i in range(num_baselines):
        b = baseline_locs[i]
        a = np.zeros(2)
        dist = np.sqrt(np.sum(np.power(b, 2)))
        if 0 < dist < thresh:
            a[0] = ant1[i]
            a[1] = ant2[i]
            pairs.append(a)
            weights.append(1 / dist)
    return weights, pairs


def moran_corr(x, weights, pairs):
    N = len(weights)
    x_avg = np.mean(x)
    sum_up = 0
    W = np.sum(weights)
    sum_down = np.sum(np.power(x - x_avg, 2))
    for i in range(N):
        sum_up = sum_up + weights[i] * (x[int(pairs[i][0])] - x_avg) * (x[int(pairs[i][1])] - x_avg)
    moran_i = (N * sum_up) / (W * sum_down)
    return moran_i


def spc_flagger(x1, x2, weights, pairs, flags, thresh_val, num_freq, mode, time):
    state = np.zeros(num_freq)
    if mode == "default":
        for j in range(num_freq):
            # print(moran_corr(x2[:, j], weights, pairs))
            flags[time][j] = int(moran_corr(x2[:, j], weights, pairs) > thresh_val)
    if mode == "differential":
        x_diff = abs(x2 - x1)
        for j in range(num_freq):
            flags[time][j] = int((moran_corr(x_diff[:, j], weights, pairs) > thresh_val and np.sum(x_diff[:, j]) > 0)
                                 or (moran_corr(x_diff[:, j], weights, pairs) < thresh_val and state[j] == 1))
            state[j] = flags[time][j]
    if mode == "hybrid":
        x_diff = abs(x2 - x1)
        for j in range(num_freq):
            flags[time][j] = int((moran_corr(x_diff[:, j], weights, pairs) > thresh_val and np.sum(x_diff[:, j]) > 0)
                                 or (moran_corr(x_diff[:, j], weights, pairs) < thresh_val and state[j] == 1) or
                                 moran_corr(x2[:, j], weights, pairs) > thresh_val)
            state[j] = flags[time][j]


weights, pairs = weight_gen(baselines, ant_id1, ant_id2, 500)

start_time = 0
length = 84
mode = "hybrid"
flags = np.zeros((length, num_freqs))
threshold = 0.75

for i in range(1, length):
    print(i)
    vis2 = abs(vis.getcol('DATA', i * num_baselines, num_antennas))
    vis1 = abs(vis.getcol('DATA', (i - 1) * num_baselines, num_antennas))
    spc_flagger(vis1[:, :, 0], vis2[:, :, 0], weights, pairs, flags, threshold, num_freqs, mode, i)

print(np.sum(flags))
