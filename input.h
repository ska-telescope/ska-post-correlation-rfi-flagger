#ifndef SKA_POST_CORRELATION_RFI_FLAGGER_INPUT_H
#define SKA_POST_CORRELATION_RFI_FLAGGER_INPUT_H



#include <casacore/tables/Tables/Table.h>
#include <casacore/tables/Tables/ScalarColumn.h>
#include <casacore/casa/config.h>
#include <casacore/tables/Tables/ArrayColumn.h>
#include <casacore/casa/Arrays/Vector.h>
#include <casacore/casa/Arrays/Slicer.h>
#include <casacore/casa/Arrays/ArrayMath.h>
#include <iostream>
#include <fstream>
#include <ctime>
#include <casacore/casa/Arrays/Matrix.h>
#include <casacore/casa/Arrays/Cube.h>





using namespace casacore;


void slice_maker2(uInt startRow, int theLength, int step, int num_freqs, Table& tab, float *spectrogram){
    Slice slice(startRow, theLength, step);
    ArrayColumn<Complex> vis_column(tab, "DATA");
    Array<Complex> vis_data = vis_column.getColumnRange(slice);
    vis_data.removeDegenerate();
    Matrix<float> amps (amplitude(vis_data));
    for (int i = 0; i < theLength; i++){
        for (int j = 0; j < num_freqs; j++){
            spectrogram[i * num_freqs + j] = *amps[i][j].data();
        }
    }
}


void slice_combiner(uInt startRow, int the_length, int step, int num_freqs, int num_baselines,
                             Table& visibilities, Table& rfi, float *spectrogram, int num_polarisation){

    Slice slice(startRow, the_length * num_baselines, step);
    ArrayColumn<Complex> vis_column(visibilities, "DATA");
    Array<Complex> vis_data = vis_column.getColumnRange(slice);
    vis_data.removeDegenerate();

    ArrayColumn<Complex> rfi_column(rfi, "DATA");
    Array<Complex> rfi_data = rfi_column.getColumnRange(slice);
    rfi_data.removeDegenerate();

    Array<Complex> received_signal = rfi_data + vis_data;

    Cube<float> amps = amplitude(received_signal);


    for (int i = 0; i < the_length; i++){
        for (int m = 0; m < num_baselines; m++){
            for (int k = 0; k < num_polarisation; k++){
                for (int j = 0; j < num_freqs; j++){
                    spectrogram[i * num_freqs * num_polarisation * num_baselines + m * num_polarisation * num_freqs
                    + k * num_freqs + j] = *amps[i * num_baselines + m][j][k].data();

                }
            }
        }
    }


}


#endif //SKA_POST_CORRELATION_RFI_FLAGGER_INPUT_H

