import casacore.tables as tbl
import numpy as np
import ska_post_correlation_rfi_flagger
from scipy.stats.mstats import winsorize


vis = tbl.table(
    '~/CLionProjects/ska-post-correlation-rfi-flagger/cmake-build-debug/MeasurementSets/20_sources_with_screen/aa0'
    '.5_ms.MS', readonly = False)
rfi = tbl.table(
    '~/CLionProjects/ska-post-correlation-rfi-flagger/cmake-build-debug/MeasurementSets/aa05_low_rfi_84chans.ms')

#col_vis = tbl.tablecolumn(vis, 'DATA')
#col_rfi = tbl.tablecolumn(rfi, 'DATA')





num_slices = 2
num_rows = vis.nrows()
num_freqs = 200
num_polarisations = 4
num_baselines = 21
initial_threshold = 40
num_seq_len = 6
sequence_lengths = np.array([1, 2, 4, 8, 16, 32], dtype=np.int32)
rho1 = 1.5
factor1 = 20

rows_per_slice = num_rows // num_slices

num_time = rows_per_slice // num_baselines


spectrogram = np.zeros([rows_per_slice, num_freqs, num_polarisations], dtype=np.float32)
flags = np.zeros([rows_per_slice, num_freqs, num_polarisations], dtype=np.int32)




def threshold_calc(spectrogram, rho, seq_lengths, factor):
    W = np.mean(winsorize(spectrogram[:,:,0], limits=[0, 0.1]))
    initial_value = factor * W
    thresholds = np.zeros(num_seq_len, dtype=np.float32)
    thresholds[0] = initial_value
    for i in range(1, len(seq_lengths)):
        m = pow(rho, np.log2(seq_lengths[i]))
        thresholds[i] = initial_value / m
    return thresholds




for s in range(num_slices):
    start_row = int(s * rows_per_slice)
    vis_data = vis.getcol('DATA', start_row, start_row + rows_per_slice)
    rfi_data = rfi.getcol('DATA', start_row, start_row + rows_per_slice)
    spectrogram = abs(vis_data + rfi_data)
    threshs = threshold_calc(spectrogram, rho1, sequence_lengths,factor1)
    ska_post_correlation_rfi_flagger.run_flagger_on_all_slices(num_time, num_freqs, num_baselines,
                                                                    num_polarisations,
                                                                    spectrogram, flags,
                                                                    threshs, num_seq_len, sequence_lengths)
    vis.putcol('FLAG', flags, start_row, rows_per_slice, 1)


vis_data_ft = vis.getcol('DATA', 0, num_rows)
rfi_data_ft = rfi.getcol('DATA', 0, num_rows)

flags_ft = vis.getcol('FLAG', 0, num_rows)

false_positive = 0
true_positive = 0
false_negative = 0
true_negative = 0

for i in range(num_rows):
    for j in range(num_freqs):
        for k in range(num_polarisations):
            r = abs(rfi_data_ft[i][j][k])
            v = abs(vis_data_ft[i][j][k])
            tot = r + v
            f = flags_ft[i][j][k]
            if f and r > 0:
                true_positive = true_positive + 1
            if f and r == 0:
                false_positive = false_positive + 1
            if not f and r > 0:
                false_negative = false_negative + 1
            if not f and r == 0:
                true_negative = true_negative + 1

print("TP: ", true_positive)
print("FP: ", false_positive)
print("FN: ", false_negative)
print("TN: ", true_negative)


# # print(flags_ft[4976][131][3])












