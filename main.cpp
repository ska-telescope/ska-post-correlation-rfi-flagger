#include <casacore/tables/Tables/Table.h>
#include <casacore/tables/Tables/ScalarColumn.h>
#include <casacore/casa/config.h>
#include <casacore/tables/Tables/ArrayColumn.h>
#include <casacore/casa/Arrays/Vector.h>
#include <casacore/casa/Arrays/Slicer.h>
#include <casacore/casa/Arrays/ArrayMath.h>
#include <iostream>
#include <fstream>
#include <ctime>
#include <casacore/casa/Arrays/Matrix.h>
#include <casacore/casa/Arrays/Cube.h>
#include "input.h"
#include "output.h"
#include "algorithm.h"
#include "performance.h"

using namespace casacore;


int main() {
    Table vis ("MeasurementSets/20_sources_with_screen/aa0.5_ms.MS", Table::Update); //table is opened
    Table rfi ("MeasurementSets/aa05_low_rfi_84chans.ms");
    int total_number_of_rows = vis.nrow();
    int observation_time = 2;
    int rows_per_second = total_number_of_rows / observation_time;
    int window_in_seconds = 1;
    int rows_per_slice = window_in_seconds * rows_per_second;
    int number_of_slices = total_number_of_rows /rows_per_slice;
    int count_remaining_slices = number_of_slices;
    int count_processed_rows = 0;
    std::clock_t c_previous = std::clock();
    uInt start_row = 0;
    int counter = 0;
    float factor = 4;
    int num_freqs = 200;
    int num_baselines = 21;
    int the_length = rows_per_slice / num_baselines;
    int num_seq_len = 6;
    float rho = 1.5;
    int num_polarisation = 4;
    std::cout << the_length << std::endl;

    float initial_threshold = 15;



   int seqlen[6] = {1, 2, 4, 8, 16, 32}; // sequence lengths of interest



    float* spectrogram = new float [num_freqs * the_length * num_polarisation * num_baselines];
    int* flags = new int [num_freqs * the_length * num_polarisation * num_baselines];
    float* thresholds = new float [6];





    threshold_calculator(initial_threshold, rho, seqlen, thresholds, 6);



    // gradually fetches the slices. The actual length of the simulations is enforced by the sleep method
    while (count_remaining_slices > 0){
        std::clock_t c_now = std::clock();
        if ((c_now - c_previous) < window_in_seconds * CLOCKS_PER_SEC){
            sleep(window_in_seconds - (c_now - c_previous)/CLOCKS_PER_SEC); // sleep until next batch
            slice_combiner(start_row, the_length, 1, num_freqs, num_baselines, vis, rfi, spectrogram, num_polarisation);

            run_flagger_on_all_slices(the_length, num_freqs, num_baselines, num_polarisation, spectrogram,
                                      flags, thresholds, num_seq_len, seqlen);
            write_to_the_table(start_row, the_length, num_polarisation, num_baselines, num_freqs, vis, flags);


            c_previous = c_now;
            start_row += rows_per_slice;
            count_remaining_slices--;
            counter++;
        } else{
            slice_combiner(start_row, the_length, 1, num_freqs, num_baselines, vis, rfi, spectrogram, num_polarisation);
            run_flagger_on_all_slices(the_length, num_freqs, num_baselines, num_polarisation, spectrogram,
                                      flags, thresholds, num_seq_len, seqlen);
            write_to_the_table(start_row, the_length, num_polarisation, num_baselines, num_freqs, vis, flags);
            c_previous = c_now;
            start_row += the_length;
            count_remaining_slices--;
        }
    }

    delete[] spectrogram;
    delete[] flags;
    delete[] thresholds;
    performance_measurement(rfi, vis, num_freqs, total_number_of_rows, num_polarisation);

 //   performance_eval("mydata_1h.csv", vis, 3600, 129, 384, 21);
    return 0;
}

