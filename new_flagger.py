import casacore.tables as tables
import numpy as np
from scipy.interpolate import lagrange

vis = tables.table('~/CLionProjects/ska-post-correlation-rfi-flagger/cmake-build-debug/MeasurementSets/20_sources_no_screen/'
                   'aa0.5_ms_without_screens.MS', readonly = False)
rfi = tables.table('~/CLionProjects/ska-post-correlation-rfi-flagger/cmake-build-debug/MeasurementSets/aa05_low_rfi_84chans.ms')

num_rows = vis.nrows()
vis_data = vis.getcol('DATA', 0, num_rows)
rfi_data = rfi.getcol('DATA', 0, num_rows)
rfi_threshld = 0

number_of_samples = 10
num_baselines = 21
num_antennas = 6
num_pols = 4
num_time_samples = int(num_rows/num_baselines)

num_freqs = 200

rec_data_abs = abs(vis_data + rfi_data)
rfi_abs = abs(rfi_data)
vis_abs = abs(vis_data)

baselines = vis.getcol('UVW', 0, num_baselines )
ant_id1 = vis.getcol('ANTENNA1', 0, num_baselines)
ant_id2 = vis.getcol('ANTENNA2', 0, num_baselines)

interpolation_window_size = 4

antenna_indices = np.array([0, 6, 11, 15, 18, 20])
factor = 0.015



flags = np.zeros((num_rows, num_freqs, num_pols))

for ant in antenna_indices:
    for j in range(num_freqs):
        state = np.zeros(num_time_samples)
        for i in range(1, num_time_samples):
            var = (rec_data_abs[i * num_baselines + ant][j][0] - rec_data_abs[(i - 1) * num_baselines + ant][j][0])/rec_data_abs[(i - 1) * num_baselines + ant][j][0]
            if var > factor  or (state[i - 1] == 1 and abs(var) <  factor):
                state[i] = 1
                for b in range(num_baselines):
                    flags[i * num_baselines + b][j][0] = 1
                    flags[i * num_baselines + b][j][1] = 1
                    flags[i * num_baselines + b][j][2] = 1
                    flags[i * num_baselines + b][j][3] = 1
            if var < 0 and abs(var) > factor and state[i - 1] == 0:
                m = i - 1
                while m > 0 and state[m] == 0:
                    for b in range(num_baselines):
                        flags[m * num_baselines + b][j][0] = 1
                        flags[m * num_baselines + b][j][1] = 1
                        flags[m * num_baselines + b][j][2] = 1
                        flags[m * num_baselines + b][j][3] = 1
                    state[m] = 1
                    m = m - 1








            # if rfi_abs[i * num_baselines + ant][j][0] > 0 and state[i] == 0:
            #     print(var, "  ", rfi_abs[i * num_baselines + ant][j][0], "  ", state[i - 1], "  ", state[i - 2], "  ", i)




        # r1 = rec_data_abs_fake[(i - 1) * num_baselines + ant][j][0] - rec_data_abs_fake[(i - 2) * num_baselines + ant][j][0]
            # r2 = rec_data_abs_fake[(i - 2) * num_baselines + ant][j][0] - rec_data_abs_fake[(i - 3) * num_baselines + ant][j][0]
            # r = r1 - r2
            # pr = r1 + r
            # predicted_value = pr + rec_data_abs_fake[(i - 1) * num_baselines + ant][j][0]
            # var = rec_data_abs[i * num_baselines + ant][j][0] - predicted_value
            # print(var)
            # if var > factor * pr:
            #     rec_data_abs_fake[i * num_baselines + ant][j][0] = predicted_value
            #     for b in range(num_baselines):
            #         flags[i * num_baselines + b][j][0] = 1
            #         flags[i * num_baselines + b][j][1] = 1
            #         flags[i * num_baselines + b][j][2] = 1
            #         flags[i * num_baselines + b][j][3] = 1





            # if var > factor * predicted_value:
            #     state = 1
            #     rec_data_abs_fake[i * num_baselines + ant][j][0] = predicted_value
            #     for b in range(num_baselines):
            #         flags[i * num_baselines + b][j][0] = 1
            #         flags[i * num_baselines + b][j][1] = 1
            #         flags[i * num_baselines + b][j][2] = 1
            #         flags[i * num_baselines + b][j][3] = 1
            # elif state == 1 and abs(var) < factor * predicted_value:
            #     rec_data_abs_fake[i * num_baselines + ant][j][0] = predicted_value
            #     for b in range(num_baselines):
            #         flags[i * num_baselines + b][j][0] = 1
            #         flags[i * num_baselines + b][j][1] = 1
            #         flags[i * num_baselines + b][j][2] = 1
            #         flags[i * num_baselines + b][j][3] = 1
            # elif state == 1 and abs(var) > factor * predicted_value and var < 0:
            #     state = 0
            # if rfi_abs[i * num_baselines + ant][j][0] > 0:
            #     print(state, "  ", rec_data_abs[i * num_baselines + ant][j][0], "  ", predicted_value)
            # if state == 1 and rfi_abs[i * num_baselines + ant][j][0] > 0:
            #     detect = "  TP"
            # if state == 0 and rfi_abs[i * num_baselines + ant][j][0] > 0:
            #     detect = "  FN"
            # if state == 0 and rfi_abs[i * num_baselines + ant][j][0] == 0:
            #     detect = "  FP"




















# x = np.zeros(interpolation_window_size)
# for i in range(interpolation_window_size):
#     x[i] = i

# state = 0
# for ant in antenna_indices:
#     for j in range(num_freqs):
#         for i in range(interpolation_window_size, num_time_samples):
#             interpolation_window = []
#             for w in range(interpolation_window_size):
#                 interpolation_window.append(rec_data_abs[(i - interpolation_window_size + w) * num_baselines + ant][j][0])
#             poly = lagrange(x, interpolation_window)
#             if rec_data_abs[i * num_baselines + ant][j][0] - poly(interpolation_window_size) > \
#                 factor * poly(interpolation_window_size) and abs(poly(interpolation_window_size)) < 50 * rec_data_abs[i * num_baselines + ant][j][0]:
#                 # if rfi_abs[i * num_baselines + ant][j][0] == 0:
#                 #     print(ant, "  ", j, "  ", i)
#                 #     print("x = ", x)
#                 #     print(interpolation_window)
#                 #     print(rec_data_abs[i * num_baselines + ant][j][0], "  ", poly(interpolation_window_size), "  ")
#                 for b in range(num_baselines):
#                     flags[i * num_baselines + b][j][0] = 1
#                     flags[i * num_baselines + b][j][1] = 1
#                     flags[i * num_baselines + b][j][2] = 1
#                     flags[i * num_baselines + b][j][3] = 1
#                 if i > 0:
#                     rec_data_abs[i * num_baselines + ant][j][0] = poly(interpolation_window_size)
#
#
#

TP = 0
FP = 0
TN = 0
FN = 0
rfi_threshold = 0
for i in range(num_rows):
    for j in range(num_freqs):
        for k in range(num_pols):
            if  flags[i][j][k] == 1 and rfi_abs[i][j][k] > rfi_threshold * rec_data_abs[i][j][k]:
                TP = TP + 1
            if  flags[i][j][k] == 1 and rfi_abs[i][j][k] <= rfi_threshold * rec_data_abs[i][j][k]:
                FP = FP + 1
            if  flags[i][j][k] == 0 and rfi_abs[i][j][k] > rfi_threshold * rec_data_abs[i][j][k]:
                FN = FN + 1
            if  flags[i][j][k] == 0 and rfi_abs[i][j][k] <= rfi_threshold * rec_data_abs[i][j][k]:
                TN = TN + 1

print("TP: ", TP/(TP + FN) * 100, "%")
print("FP: ", FP/(FP + TN) * 100, "%")








