import casacore.tables as tables
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import lagrange
import numpy.polynomial.polynomial as ply
from numpy.polynomial.polynomial import Polynomial

vis = tables.table('~/CLionProjects/ska-post-correlation-rfi-flagger/cmake-build-debug/MeasurementSets/20_sources_no_screen/'
                   'aa0.5_ms_without_screens.MS', readonly = False)
rfi = tables.table('~/CLionProjects/ska-post-correlation-rfi-flagger/cmake-build-debug/MeasurementSets/aa05_low_rfi_84chans.ms')

num_rows = vis.nrows()
vis_data = vis.getcol('DATA', 0, num_rows)
rfi_data = rfi.getcol('DATA', 0, num_rows)

number_of_samples = 10
num_baselines = 21
num_antennas = 6
num_pols = 4
num_time_samples = int(num_rows/num_baselines)

num_freqs = 200

rec_data_abs = abs(vis_data + rfi_data)
rfi_abs = abs(rfi_data)
vis_abs = abs(vis_data)

baselines = vis.getcol('UVW', 0, num_baselines )
ant_id1 = vis.getcol('ANTENNA1', 0, num_baselines)
ant_id2 = vis.getcol('ANTENNA2', 0, num_baselines)



distances = np.zeros((num_antennas, num_antennas))

for i in range(num_baselines):
    b = baselines[i]
    distances[ant_id1[i], ant_id2[i]] = np.sqrt(np.sum(np.power(b, 2)))
    distances[ant_id2[i], ant_id1[i]] = np.sqrt(np.sum(np.power(b, 2)))



def moran_auto_correlation(weights, x):
    N = len(x)
    x_avg = np.sum(x)/N
    sum_up = 0
    W = np.sum(weights)
    sum_down = np.sum(np.power(x - x_avg, 2))
    for i in range(N):
        for j in range(N):
            sum_up = sum_up + weights[i][j] * (x[i] - x_avg) * (x[j] - x_avg)
    moran_i = (N * sum_up) / (W * sum_down)
    return moran_i

def geary_auto_correlation(weights, x):
    N = len(x)
    x_avg = np.sum(x)/N
    sum_up = 0
    W = np.sum(weights)
    sum_down = np.sum(np.power(x - x_avg, 2))
    for i in range(N):
        for j in range(N):
            sum_up = sum_up + weights[i][j] * pow(x[i] - x[j], 2)
    geary = ((N-1) * sum_up) / (2 * W * sum_down)
    return geary



def binary_weights(distance, threshold):
    s = np.shape(distance)
    weights = np.zeros(s)
    for i in range(s[0]):
        for j in range(s[1]):
            if threshold > distance[i][j] > 0:
                weights[i][j] = 1
    return weights

def inverse_dist_weight(distance):
    s = np.shape(distance)
    weights = np.zeros(s)
    for i in range(s[0]):
        for j in range(s[1]):
            if distance[i][j] > 0:
                weights[i][j] = 1/distance[i][j]
    return weights

interpolation_window_size = 5
interpolation_window = np.zeros(interpolation_window_size)

x = np.zeros(interpolation_window_size)
for i in range(interpolation_window_size):
    x[i] = i

for m in range(1):
    for j in range(1):
        for i in range(num_time_samples):
            for w in range(min(i, interpolation_window_size)):
                interpolation_window[w] = vis_abs[(i - interpolation_window_size + w) *  num_baselines + m][j][0]
            poly = lagrange(x, interpolation_window)



         #   print(((polyv - vis_abs[i * num_baselines + m][j][0])/vis_abs[i * num_baselines + m][j][0]) * 100)









# antenna_indices = np.array([0, 6, 11, 15, 18, 20])
# weights = inverse_dist_weight(distances)
# #weights = binary_weights(distances, 50)
# moran_indices = np.zeros((num_time_samples, num_freqs, num_pols))
#
# S0 = 0
# k0 = 0
# S1 = 0
# k1 = 0
# without_rfi = []
# with_rfi = []
# flags = np.zeros((num_rows, num_freqs, num_pols))
# vals1 = np.zeros(num_antennas)
#
# for i in range(num_time_samples):
#     for j in range(num_freqs):
#         for k in range(num_pols):
#             start_row = i * num_baselines
#             vals = np.zeros(num_antennas)
#             for m in range(num_antennas):
#                 vals[m] = rec_data[start_row + antenna_indices[m]][j][k]
#                 # if rfi_abs[start_row + antenna_indices[m]][j][k] > 0:
#                 #     R = 1
#             moran_indices[i][j][k] = moran_auto_correlation(weights, vals)
#             for b in range(num_baselines):
#                 if rfi_abs[i * num_baselines + b][j][k] == 0:
#                     S0 = S0 + abs(moran_indices[i][j][k])
#                     k0 = k0 + 1
#                     without_rfi.append(moran_indices[i][j][k])
#                 if rfi_abs[i * num_baselines + b][j][k] > 0:
#                     S1 = S1 + abs(moran_indices[i][j][k])
#                     k1 = k1 + 1
#                     with_rfi.append(moran_indices[i][j][k])
#
#
#
#
# plt.hist(without_rfi, color='g', label='without rfi', bins=100, histtype='step')
# plt.hist(with_rfi, color='b', label='with rfi', bins=100, histtype='step')
# plt.title('Distribution of Moran I index')
#
# plt.legend()
# plt.show()
#
# # PART II
#
# min_change = 0.5
# moran_threshold1 = 0.2
# moran_threshold2 = 0.5
# switch = 0
# last_switch_point = 0
# natural_growth_rate = 0.1
# window_size = 3
# decision_factor = 10
# flags = np.zeros((num_rows, num_freqs, num_pols))
#
#
#
# for j in range(num_freqs):
#     for m in range(num_antennas):
#         baseline_id = antenna_indices[m]
#         switch = 0
#         natural_growth_rate = 0
#         for i in range(2, num_time_samples):
#             r0 = abs(rec_data[i * num_baselines + baseline_id][j][0])
#             r1 = abs(rec_data[(i - 1) * num_baselines + baseline_id][j][0])
#             r2 = abs(rec_data[(i - 2) * num_baselines + baseline_id][j][0])
#             r3 = abs(rec_data[(i - 3) * num_baselines + baseline_id][j][0])
#             r4 = abs(rec_data[(i - 4) * num_baselines + baseline_id][j][0])
#             r5 = abs(rec_data[(i - 5) * num_baselines + baseline_id][j][0])
#             natural_growth_rate = min(abs(r1 - r2), abs(r2 - r3), abs(r3 - r4), abs(r4 - r5))
#
#             if r0 - r1 > 0 and r0 - r1 > decision_factor * natural_growth_rate:
#                 switch = 1
#
#             if r0 - r1 < 0 and abs(r0 - r1) > decision_factor * natural_growth_rate:
#                 switch = 0
#             if switch == 1:
#                 for b in range(num_baselines):
#                     flags[i * num_baselines + b][j][0] = 1
#                     flags[i * num_baselines + b][j][1] = 1
#                     flags[i * num_baselines + b][j][2] = 1
#                     flags[i * num_baselines + b][j][3] = 1
#             # if switch == 0 and rfi_abs[i * num_baselines + baseline_id][j][0] > 0:
#             #     print(i, "  ", j, "  ", r0 - r1, "  ", r1 - r2,"  ", r2 - r3, "  ", r3 - r4, " ", r4 - r5, "  ", natural_growth_rate)

























# for b in range(num_baselines):
#     for j in range(num_freqs):
#        for i in range(1, num_time_samples - 1):
#            s1 = abs(rec_data[i * num_baselines + b][j][0]) - abs(rec_data[(i - 1) * num_baselines + b][j][0])
#            s2 = abs(rec_data[(i + 1) * num_baselines + b][j][0]) - abs(rec_data[i * num_baselines + b][j][0])
#            s = abs(s1) + abs(s2)
#            if s1 > min_change * abs(rec_data[i * num_baselines + b][j][0]):
#                switch = 1
#            if s1 < -min_change * abs(rec_data[i * num_baselines + b][j][0]):
#                switch = 0
#            if  moran_indices[i][j][0] > moran_threshold2 or (switch == 1 and moran_threshold1 < moran_indices[i][j][0] < moran_threshold2):
#                flags[i * num_baselines + b][j][0] = 1
#                flags[i * num_baselines + b][j][1] = 1
#                flags[i * num_baselines + b][j][2] = 1
#                flags[i * num_baselines + b][j][3] = 1


            # or (switch == 1 and moran_threshold1 < moran_indices[i][j][0] < moran_threshold2):
            # s = 0
            # (s1 > min_change and moran_indices[i][j][0] > moran_threshold1) or
            # for t in range(i, i+W-1):
            #         s = s + abs(abs(vis_data[(t + 1) * num_baselines + b][j][0]) - abs(vis_data[t * num_baselines + b][j][0]))
            # if rfi_abs[(i + 1) * num_baselines + b][j][0] == 0:
            #     print(s)
            # if  s > min_change * W and moran_indices[t][j][0] > moran_threshold:
            #     flags[(i + 1) * num_baselines + b][j][0] = 1
            #     flags[(i + 1) * num_baselines + b][j][1] = 1
            #     flags[(i + 1) * num_baselines + b][j][2] = 1
            #     flags[(i + 1) * num_baselines + b][j][3] = 1

# TP = 0
# FP = 0
# TN = 0
# FN = 0
# rfi_threshold = 0.1
# for i in range(num_rows):
#     for j in range(num_freqs):
#         for k in range(num_pols):
#             if  flags[i][j][k] == 1 and rfi_abs[i][j][k] > 0:
#                 TP = TP + 1
#                # print(abs(rec_data[i][j][k]), " ", abs(vis_data[i][j][k]), " ", abs(rfi_data[i][j][k]), " ", i, " ", j, " ", k)
#             if  flags[i][j][k] == 1 and rfi_abs[i][j][k] < rfi_threshold * rec_data[i][j][k]:
#                 FP = FP + 1
#             if  flags[i][j][k] == 0 and rfi_abs[i][j][k] > rfi_threshold * rec_data[i][j][k]:
#                 FN = FN + 1
#             if  flags[i][j][k] == 0 and rfi_abs[i][j][k] < rfi_threshold * rec_data[i][j][k]:
#                 TN = TN + 1
#
# print("TP: ", TP/(TP + FN) * 100, "%")
# print("FP: ", FP/(FP + TN) * 100, "%")

# delays = np.zeros(num_time_samples)
# little_flags = np.zeros(num_time_samples)
# freq = 88
# pol = 0
# baseline = 8
#
#
# delay_rfi = []
# delay_norfi = []
# rfi_time_stamp = []
#
# for i in range(1, num_time_samples):
#     if rfi_abs[i * num_baselines + baseline][freq][pol] > 0:
#         delay_rfi.append(abs(rec_data[i * num_baselines + baseline][freq][pol]) - abs(rec_data[(i - 1) * num_baselines + baseline][freq][pol]))
#         rfi_time_stamp.append(i)
#
#
# for i in range(1, num_time_samples):
#     delays[i] = abs(rec_data[i * num_baselines + baseline][freq][pol]) - abs(rec_data[(i - 1) * num_baselines + baseline][freq][pol])
#     if rfi_abs[i * num_baselines + baseline][freq][pol] > 0:
#         little_flags[i] = 1
#    # print(delays[i], " ", little_flags[i])
#
# plt.plot(delays, 'bo--', label='Good signal')
# plt.plot(rfi_time_stamp, delay_rfi, 'ro', label='RFI contaminated')
# plt.title('differences between successive samples in time domain-baseline(1,3)')
# plt.legend()
# plt.show()








#
#
#
#
# plt.hist(without_rfi, color='g', label='without rfi', bins=100, histtype='step', cumulative=True, density=True)
# plt.hist(with_rfi, color='b', label='with rfi', bins=100, histtype='step', cumulative=True, density=True)
# plt.title('20-sources')
#
# plt.legend()
# plt.show()
#

























