import casacore.tables as tables
import numpy as np

# import datetime

vis = tables.table('/mnt/RFI1/MeerKAT/Nest200047/1643124898_sdp_l0.ms')

# start_time = datetime.datetime.now()
num_rows = vis.nrows()
rfi_threshld = 0.02

batch_length = 10
num_baselines = 1830
num_antennas = 60
num_pols = 4
num_time_samples = int(num_rows / num_baselines)

num_freqs = 4096

baselines = vis.getcol('UVW', 0, num_baselines)
ant_id1 = vis.getcol('ANTENNA1', 0, num_baselines)
ant_id2 = vis.getcol('ANTENNA2', 0, num_baselines)

distances = np.zeros((num_antennas, num_antennas))

for i in range(num_baselines):
    b = baselines[i]
    distances[ant_id1[i], ant_id2[i]] = np.sqrt(np.sum(np.power(b, 2)))
    distances[ant_id2[i], ant_id1[i]] = np.sqrt(np.sum(np.power(b, 2)))


def moran_auto_correlation(weights, x):
    N = len(x)
    x_avg = np.sum(x) / N
    sum_up = 0
    W = np.sum(weights)
    sum_down = np.sum(np.power(x - x_avg, 2))
    for i in range(N):
        for j in range(N):
            sum_up = sum_up + weights[i][j] * (x[i] - x_avg) * (x[j] - x_avg)
    moran_i = (N * sum_up) / (W * sum_down)
    return moran_i


def binary_weights(distance, threshold):
    s = np.shape(distance)
    weights = np.zeros(s)
    for i in range(s[0]):
        for j in range(s[1]):
            if threshold > distance[i][j] > 0:
                weights[i][j] = 1
    return weights


def inverse_dist_weight(distance):
    s = np.shape(distance)
    weights = np.zeros(s)
    for i in range(s[0]):
        for j in range(s[1]):
            if distance[i][j] > 0:
                weights[i][j] = 1 / distance[i][j]
    return weights


def spc_flagger(vis_data, mode, num_time, num_bases, num_freq, ant_ids, weights, threshold):
    flags = np.zeros(vis_data.shape[:, :, 0])
    if mode == "default":
        for i in range(1, num_time):
            for j in range(num_freq):
                vec = []
                for ant in ant_ids:
                    vec.append(vis_data[i * num_bases + ant][j][0])
                    moran = moran_auto_correlation(weights, vec)
                    if moran > threshold:
                        for b in range(num_bases):
                            flags[i * num_bases + b][j][0] = 1
    return flags









b_ids = []
for i in range(num_baselines):
    if ant_id1[i] == ant_id2[i]:
        b_ids.append(i)

interpolation_window_size = 4

antenna_indices = np.array(b_ids)

factor = 0.1


def twosmf(vis_data_abs):
    visvis = vis_data_abs[:, :, 0]
    [num_time, num_freq] = visvis.shape
    flags = np.zeros((num_time, num_freq))
    s = 0
    tot = num_time * num_freq
    for j in range(num_freq):
        state = np.zeros(num_time)
        for i in range(1, num_time):
            var = (visvis[i][j] - visvis[i - 1][j]) / visvis[i - 1][j]
            if var > factor or (state[i - 1] == 1 and abs(var) < factor):
                state[i] = 1
                flags[i][j] = 1
                s = s + 1
            if var < 0 and abs(var) > factor and state[i - 1] == 0:
                m = i - 1
                while m > 0 and state[m] == 0:
                    flags[m][j] = 1
                    state[m] = 1
                    s = s + 1
                    m = m - 1

    print(s, "  ", tot, "  ", (s / tot) * 100, "%")
    return flags


def autocorrvals(vis_data, num_antennas, freq):
    a = []
    for i in range(num_antennas):
        a.append(vis_data[i][freq][0])
    print(a)


point_of_time = 27
visvis1 = vis.getcol('DATA', point_of_time * num_baselines, num_antennas)
autocorrvals(abs(visvis1), num_antennas, 185)


start_time = 1604
length_of_time = 147
baseline_id = 47

scan_number = vis.getcol('SCAN_NUMBER', baseline_id, num_time_samples, num_baselines)
field_id = vis.getcol('FIELD_ID', baseline_id, num_time_samples, num_baselines)

change_locations = []
for i in range(1, num_time_samples):
    if scan_number[i] != scan_number[i - 1] or field_id[i] != field_id[i - 1]:
        change_locations.append(i)

visvis2 = abs(vis.getcol('DATA', start_time * num_baselines, length_of_time * num_baselines))

weights = binary_weights(distances, 10)
threshold = 0.9

spc_flags = spc_flagger(visvis2, "default", length_of_time, num_baselines, num_freqs, antenna_indices, weights, threshold)

# print(change_locations)
#
start_row = start_time * num_baselines + baseline_id

vis_data_abs = abs(vis.getcol('DATA', start_row, length_of_time, num_baselines))
table_flags = vis.getcol('FLAG', start_row, length_of_time, num_baselines)
flags = twosmf(vis_data_abs)

np.savetxt("results/spc_flags.txt", spc_flags)
np.savetxt("results/spc_vis.txt", visvis2)

# np.savetxt("results/observation1_ant47_visibility10.txt", vis_data_abs[:, :, 0])
# np.savetxt("results/observation1_ant47_new_flags10.txt", flags)


# np.savetxt("results/observation0_ant5_old_flags.txt", table_flags[:, :, 0])
