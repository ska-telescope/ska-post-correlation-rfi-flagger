import casacore.tables as tables
import numpy as np

# import datetime

vis = tables.table('/mnt/RFI1/MeerKAT/Nest200047/1643124898_sdp_l0.ms')

# start_time = datetime.datetime.now()
num_rows = vis.nrows()
rfi_threshld = 0.02

batch_length = 10
num_baselines = 1830
num_antennas = 60
num_pols = 4
num_time_samples = int(num_rows / num_baselines)

num_freqs = 4096

baselines = vis.getcol('UVW', 0, num_baselines)
ant_id1 = vis.getcol('ANTENNA1', 0, num_baselines)
ant_id2 = vis.getcol('ANTENNA2', 0, num_baselines)

distances = np.zeros((num_antennas, num_antennas))

for i in range(num_baselines):
    b = baselines[i]
    distances[ant_id1[i], ant_id2[i]] = np.sqrt(np.sum(np.power(b, 2)))
    distances[ant_id2[i], ant_id1[i]] = np.sqrt(np.sum(np.power(b, 2)))

b_ids = []
for i in range(num_baselines):
    if ant_id1[i] == ant_id2[i]:
        b_ids.append(i)

antenna_indices = np.array(b_ids)


def extrapolator(x):
    diff11 = x[2] - x[1]
    diff12 = x[1] - x[0]
    diff2 = diff11 - diff12
    diff_e = diff11 + diff2
    x_e = x[2] + diff_e
    return x_e


def extrapol_flagger(vis_data, num_freq, num_time, threshold):
    flags = np.zeros((num_time, num_freq))
    for j in range(num_freq):
        for i in range(3, num_time):
            x = np.zeros(3)
            x[0] = vis_data[i - 3][j][0]
            x[1] = vis_data[i - 2][j][0]
            x[2] = vis_data[i - 1][j][0]
            x_e = extrapolator(x)
            if (vis_data[i][j][0] - x_e) / x_e > threshold:
                flags[i][j] = 1
            if (vis_data[i][j][0] - x_e) / x_e < -threshold:
                k = 1
                while flags[i - k][j] == 0 and i - k > 0:
                    flags[i - k][j] = 1
                    k = k + 1
    return flags


threshold = 0.05
start_time = 1604
length_of_time = 147
baseline_id = 47
start_row = start_time * num_baselines + baseline_id
vis_data_abs = abs(vis.getcol('DATA', start_row, length_of_time, num_baselines))
flags = extrapol_flagger(vis_data_abs, num_freqs, length_of_time, threshold)
np.savetxt("results/extrapol_obs1_ant_47_t05.txt", flags)
