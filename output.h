#ifndef SKA_POST_CORRELATION_RFI_FLAGGER_OUTPUT_H
#define SKA_POST_CORRELATION_RFI_FLAGGER_OUTPUT_H




#include <casacore/tables/Tables/Table.h>
#include <casacore/tables/Tables/ScalarColumn.h>
#include <casacore/casa/config.h>
#include <casacore/tables/Tables/ArrayColumn.h>
#include <casacore/casa/Arrays/Vector.h>
#include <casacore/casa/Arrays/Slicer.h>
#include <casacore/casa/Arrays/ArrayMath.h>
#include <iostream>
#include <fstream>
#include <ctime>
#include <casacore/casa/Arrays/Matrix.h>
#include <casacore/casa/Arrays/Cube.h>


using namespace casacore;

void write_to_the_table(uInt start_row, int the_length, int num_polarisation, int num_baselines, int num_freqs, Table& tab, int* flags){
    ArrayColumn<bool> flags_column(tab, "FLAG");
    IPosition I = IPosition(2, num_polarisation, num_freqs);
    int current_row = 0;

    for (int i = 0; i < the_length; i++){
        for (int m = 0; m < num_baselines; m++){
            bool A[num_polarisation][num_freqs];
            for (int k = 0; k < num_polarisation; k++){
                for (int j = 0; j < num_freqs; j++){
                    A[k][j] = (bool) flags[i * num_freqs * num_polarisation * num_baselines +
                                           m * num_freqs * num_polarisation + k * num_freqs + j];
                }
            }
            current_row = start_row + i * num_baselines + m;
            flags_column.put(current_row, Array<bool> (I, *A));

        }
    }


}

#endif //SKA_POST_CORRELATION_RFI_FLAGGER_OUTPUT_H