#ifndef SKA_POST_CORRELATION_RFI_FLAGGER_PERFORMANCE_H
#define SKA_POST_CORRELATION_RFI_FLAGGER_PERFORMANCE_H



#include <casacore/tables/Tables/Table.h>
#include <casacore/tables/Tables/ScalarColumn.h>
#include <casacore/casa/config.h>
#include <casacore/tables/Tables/ArrayColumn.h>
#include <casacore/casa/Arrays/Vector.h>
#include <casacore/casa/Arrays/Slicer.h>
#include <casacore/casa/Arrays/ArrayMath.h>
#include <iostream>
#include <fstream>
#include <ctime>
#include <casacore/casa/Arrays/Matrix.h>
#include <casacore/casa/Arrays/Cube.h>


using namespace casacore;


void performance_measurement(Table& rfi, Table vis, int num_freqs, int num_rows,int num_polarisation){
    int total_count = 0;
    int flags_count = 0;
    int correct_flag = 0;
    int wrong_flag = 0;
    int missed_flag = 0;


    ArrayColumn<bool> myflags_col(vis, "FLAG");
    Array<bool> myflags = myflags_col.getColumn(); //reads the flags from the table;
    Cube<bool> flags = myflags;

    ArrayColumn<Complex> rfi_column(rfi, "DATA");
    Array<Complex> rfi_signal = rfi_column.getColumn();
    Cube<float> amps_rfi = amplitude(rfi_signal);


    for (int i = 0; i < num_rows; i++){
        for (int j = 0; j < num_freqs; j++){
            for (int k = 0; k < num_polarisation; k++){
                total_count++;
                if (*flags[i][j][k].data()){
                    flags_count++;
                }
                if (*flags[i][j][k].data() && *amps_rfi[i][j][k].data() > 0){
                    correct_flag++;
                } else if (*flags[i][j][k].data() && *amps_rfi[i][j][k].data() == 0){
                    wrong_flag++;
                } else if (!*flags[i][j][k].data() && *amps_rfi[i][j][k].data() > 0){
                    missed_flag++;
                }
            }
        }
    }
    std::cout << "total number of data points   = " << total_count << std::endl;
    std::cout << "total number of flags   = " << flags_count << std::endl;
    std::cout << "number of correct flags = " << correct_flag << std::endl;
    std::cout << "number of wrong flags   = " << wrong_flag << std::endl;
    std::cout << "number of missed flags  = " << missed_flag << std::endl;
}

#endif //SKA_POST_CORRELATION_RFI_FLAGGER_PERFORMANCE_H