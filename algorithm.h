#ifndef SKA_POST_CORRELATION_RFI_FLAGGER_ALGORITHM_H
#define SKA_POST_CORRELATION_RFI_FLAGGER_ALGORITHM_H
#include<iostream>


//threshold for sequences of length 1 as a factor of mean
float initial_value(float *spectrogram, float factor, int num_freq, int the_length){
    int num_of_elements = num_freq * the_length;
    float sum = 0;
    for (int i = 0; i < num_of_elements; i++){
        sum += spectrogram[i];
    }
    return (sum/num_of_elements) * factor;
}



// rule of thumb for thresholds according to Offringa
void threshold_calculator(float init, float rho, int* seqlen, float* thresholds, int num_seq_elems){

    for (int i = 0; i < num_seq_elems; i++){
        thresholds[i] = init/(pow(rho, log2(float(seqlen[i]))));
    }
}



void sum_threshold_on_block(float* thresholds, int seqlen, int* sequence_lengths, float *block, int num_freqs, int num_time,
                            int *flags_on_block, int freq_or_time){
    float current_threshold = 0;
    int current_seqlen = 0;
    float sum = 0;

    if (!freq_or_time){
        for (int k = 0; k < seqlen; k++){
            current_seqlen = sequence_lengths[k];
            current_threshold = thresholds[k] * current_seqlen;
            for (int j = 0; j < num_freqs; j++){
                for (int i = 0; i < num_time - current_seqlen; i++){
                    sum = 0;
                    for (int m = 0; m < current_seqlen; m++){
                        sum = sum + block[(i + m) * num_freqs + j];
                    }
                    if (sum > current_threshold){
                        for (int m = 0; m < current_seqlen; m++){
                            flags_on_block[(i + m) * num_freqs + j] = 1;
                        }
                    }
                }
            }

        }
    } else if (freq_or_time){
        for (int k = 0; k < seqlen; k++){
            current_seqlen = sequence_lengths[k];
            current_threshold = thresholds[k] * current_seqlen;
            for (int i = 0; i < num_time; i++){
                for (int j = 0; j < num_freqs - current_seqlen; j++){
                    sum = 0;
                    for (int m = 0; m < current_seqlen; m++){
                        if (flags_on_block[i * num_freqs + j + m] == 1){
                            block[i * num_freqs + j + m] = thresholds[k];
                        }

                        sum = sum + block[i * num_freqs + j + m];
                    }
                    if (sum > current_threshold){
                        for (int m = 0; m < current_seqlen; m++){
                            flags_on_block[i  * num_freqs + j + m] = 1;
                        }
                    }
                }
            }

        }
    }
}

void write_flags_to_the_slice_array(int num_freqs, int num_baselines, int num_polarisation, int num_time,
                                    int baseline_id, int polarisation_id, int* flags_on_the_block, int* flags){

    for (int i = 0; i < num_time; i++){
        for (int j = 0; j < num_freqs; j++){
            flags[i * num_freqs * num_baselines * num_polarisation + baseline_id * num_freqs * num_polarisation
            + j * num_polarisation + polarisation_id] = flags_on_the_block[i * num_freqs + j];
        }
    }

}



void run_flagger_on_all_slices(int num_time, int num_freqs, int num_baselines, int num_polarisations,
                               float* spectrogram, int* flags, float* thresholds, int seqlen, int* sequence_lengths){

    float current_sum_threshold = 0;
    float current_avg_threshold = 0;
    int current_seqlen = 0;
    float sum = 0;

    for (int l = 0; l < seqlen; l++){
        current_seqlen = sequence_lengths[l];
        current_avg_threshold = thresholds[l];
        current_sum_threshold = thresholds[l] * current_seqlen;

        for (int k = 0; k < num_baselines; k++){
            for (int j = 0; j < num_freqs; j++){
                for (int i = 0; i < num_time - current_seqlen; i++){
                    sum = 0;

                    for (int m = 0; m < current_seqlen; m++){
                        /*
                        if (i == 0 && j == 36 && k == 0 && l == 0 && m == 0){
                            std::cout << spectrogram[(i + m) * num_freqs * num_baselines * num_polarisations +
                                                     k * num_freqs * num_polarisations + j * num_polarisations] << " Yes!" << std::endl;
                        }
                         */

                        if (flags[(i + m) * num_freqs * num_baselines * num_polarisations +
                                k * num_freqs * num_polarisations + j * num_polarisations] == 1){
                            spectrogram[(i + m) * num_freqs * num_baselines * num_polarisations +
                                        k * num_freqs * num_polarisations + j * num_polarisations] = current_avg_threshold;
                        }
                        sum = sum + spectrogram[(i + m) * num_freqs * num_baselines * num_polarisations +
                                                k * num_freqs * num_polarisations + j * num_polarisations];
                    }
                   // std::cout << "sum = " << sum << "  threshold = " << current_sum_threshold << std::endl;
                    if (sum > current_sum_threshold){
                        for (int n = 0; n < current_seqlen; n++){
                            for (int p = 0; p < num_polarisations; p++){
                                flags[(i + n) * num_freqs * num_baselines * num_polarisations +
                                      k * num_freqs * num_polarisations + j * num_polarisations + p] = 1;
                            }
                        }
                    }
                }
            }
        }

    }


    /*
    for (int m = 0; m < num_baselines; m++){
        for (int k = 0; k < num_polarisations; k++){
            float* block = new float[num_time * num_freqs];
            int* flags_on_the_block = new int[num_time * num_freqs];
            for (int i = 0; i < num_freqs * num_time; i++){
                flags_on_the_block[i] = 0;
            }

            for (int i = 0; i < num_time; i++){
                for (int j = 0; j < num_freqs; j++){
                    block[i * num_freqs + j] = spectrogram[i * num_freqs * num_polarisations
                                                           * num_baselines + m * num_freqs * num_polarisations + j * num_polarisations + k];
                }
            }
            sum_threshold_on_block(thresholds, seqlen, sequence_lengths, block, num_freqs, num_time, flags_on_the_block, false);

            write_flags_to_the_slice_array(num_freqs, num_baselines, num_polarisations, num_time, m, k, flags_on_the_block, flags);
            delete[] block;
            delete[] flags_on_the_block;
        }
    }
     */

}

#endif //SKA_POST_CORRELATION_RFI_FLAGGER_ALGORITHM_H


