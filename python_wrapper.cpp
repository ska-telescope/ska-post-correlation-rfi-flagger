/* See the LICENSE file at the top-level directory of this distribution. */

#include <Python.h>

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>

/* Prototypes of C function(s) to call. */
#include "algorithm.h"
#include <iostream>

static const char module_doc[] =
        "Provides an interface to the post correlation RFI flagger.";

static const char run_flagger_on_all_slices_doc[] = R"(
run_flagger_on_all_slices(num_time, num_freqs, num_baselines, num_polarisation,
                                spectrogram, flags, thresholds, sequence_lengths)
--

Add brief description of function here.

Parameters
==========
num_time (int): length of time (number of timestamps)
num_freqs (int): number of frequency channels
num_baselines (int): number of baselines
num_polarisations (int): number of polarisations (1 or 4)
spectrogram: visibility data in an array format
flags: flags in the same shape as spectrogram
thresholds: array of floats, thresholds for different sequence lengths
sequence_lengths: array of ints
)";

static PyObject* run_flagger_on_all_slices_py(PyObject* self, PyObject* args)
{
    PyObject *obj[] = {0, 0, 0, 0};
    PyArrayObject *spectrogram = 0, *flags = 0, *thresholds = 0, *sequence_lengths = 0;
    int num_time = 0;
    int num_freqs = 0;
    int num_baselines = 0;
    int num_polarisations = 0;
    int seqlen = 0;

    int num_spectrogram_dims = 0;
    int num_flags_dims = 0;
    int num_thresholds_dims = 0;
    int num_sequence_lengths_dims = 0;


    int num_rows = 0;

    npy_intp *spectrogram_dims, *flag_dims, *thresholds_dims, *sequence_lengths_dims;



    /* Parse inputs. */
    if (!PyArg_ParseTuple(args, "iiiiOOOiO",
            &num_time, &num_freqs, &num_baselines, &num_polarisations ,&obj[0], &obj[1], &obj[2], &seqlen, &obj[3]))
        return 0;

    /* Make sure input objects are arrays. */


    spectrogram = (PyArrayObject*) PyArray_FROM_O(obj[0]);
    flags= (PyArrayObject*) PyArray_FROM_O(obj[1]);
    thresholds = (PyArrayObject*) PyArray_FROM_O(obj[2]);
    sequence_lengths = (PyArrayObject*) PyArray_FROM_O(obj[3]);

    if (!spectrogram || !flags || !thresholds || !sequence_lengths) {
        goto fail;
    }

    /* Check type of arrays. */
    if (PyArray_TYPE(spectrogram) != NPY_FLOAT)
    {
        PyErr_Format(PyExc_TypeError, "Data must be of type numpy.float32");
        goto fail;
    }

    if (PyArray_TYPE(flags) != NPY_INT)
    {
        PyErr_Format(PyExc_TypeError, "Flags must be of type numpy.int32");
        goto fail;
    }

    if (PyArray_TYPE(thresholds) != NPY_FLOAT)
    {
        PyErr_Format(PyExc_TypeError, "Thresholds must be of type numpy.float32");
        goto fail;
    }

    if (PyArray_TYPE(sequence_lengths) != NPY_INT)
    {
        PyErr_Format(PyExc_TypeError, "Sequence lengths must be of type numpy.int32");
        goto fail;
    }


    /* Check dimensions of arrays. */
    num_spectrogram_dims = PyArray_NDIM(spectrogram);
    num_flags_dims = PyArray_NDIM(flags);
    num_thresholds_dims = PyArray_NDIM(thresholds);
    num_sequence_lengths_dims = PyArray_NDIM(sequence_lengths);


    if (num_spectrogram_dims != 3 || num_flags_dims != 3 || num_thresholds_dims != 1 || num_sequence_lengths_dims != 1)
    {
        PyErr_Format(PyExc_RuntimeError, "Spectrogram and flags must be two-dimensional and thresholds and sequence lengths must be one dimensional.");
        goto fail;
    }
    spectrogram_dims = PyArray_DIMS(spectrogram);
    flag_dims = PyArray_DIMS(flags);

    num_time = spectrogram_dims[0]/num_baselines;
    num_freqs = spectrogram_dims[1];
    num_polarisations = spectrogram_dims[2];


    if (num_time != flag_dims[0]/num_baselines || num_freqs != flag_dims[1] || num_polarisations != flag_dims[2])
    {
        PyErr_Format(PyExc_RuntimeError, "Inconsistent dimensions of arrays");
        goto fail;
    }
    /* Call C processing function. */
    /* Should release the Python Global Interpreter Lock here,
     * for long-running functions. */
     std::cout << "NUM IS " << num_time << std::endl;
    run_flagger_on_all_slices(num_time, num_freqs, num_baselines, num_polarisations, (float*) PyArray_DATA(spectrogram),
                               (int*) PyArray_DATA(flags), (float*) PyArray_DATA(thresholds), seqlen, (int*) PyArray_DATA(sequence_lengths));
    //sum_threshold_on_block(threshold, seq_len,
      //      (float*) PyArray_DATA(block), num_freqs, num_times,
        //    (int*) PyArray_DATA(flags_on_block), freq_or_time);


    /* Return to Python. */
    Py_XDECREF(spectrogram);
    Py_XDECREF(flags);
    Py_XDECREF(thresholds);
    Py_XDECREF(sequence_lengths);
    return Py_BuildValue(""); /* Nothing to return. */

fail:
    Py_XDECREF(spectrogram);
    Py_XDECREF(flags);
    Py_XDECREF(thresholds);
    Py_XDECREF(sequence_lengths);
    return 0;
}

/* Method table. */
static PyMethodDef methods[] =
{
        {"run_flagger_on_all_slices", (PyCFunction)run_flagger_on_all_slices_py,
                METH_VARARGS, run_flagger_on_all_slices_doc},
        {NULL, NULL, 0, NULL}
};

/* Module definition. */
static PyModuleDef moduledef = {
        PyModuleDef_HEAD_INIT,
        "_ska_post_correlation_rfi_flagger", /* m_name */
        module_doc,         /* m_doc */
        -1,                 /* m_size */
        methods             /* m_methods */
};

/* Initialisation function. */
PyMODINIT_FUNC PyInit_ska_post_correlation_rfi_flagger(void)
{
    import_array();
    PyObject* m = PyModule_Create(&moduledef);
    return m;
}
